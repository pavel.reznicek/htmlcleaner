#!/usr/bin/env python

"""Common utilities for use with the HTML Cleaner tool"""

import os

from collections import OrderedDict

import bs4

import constants

from subtraction import SubtractibleString


def tag_has_attributes(tag):
    'Checks if a bs4 tag has any attributes'
    attributes = tag.attrs
    result = len(attributes) > 0
    return result

def tag_contains_nonbreaks(tag):
    'Checks if a bs4 tag contains non-<br> tags'
    children = tag.children
    for child in children:
        if isinstance(child, bs4.Tag) and \
        child.name != 'br':
            return False
    return True

def tag_text_contains_nonwhitespace(tag):
    'Checks if a bs4 tag’s text contains anything else than whitespace'
    text = SubtractibleString(tag.text)
    # If the text contains something
    if text:
        # If the text contains non-whitespace
        # then don’t remove the tag
        remains = text.stripall(constants.WHITESPACE)
        if remains:
            return False
    return True

def comment_is_mso_conditional_code(comment):
    'Checks if a bs4 comment element is an MSO conditional code'
    string = comment.string
    if string.endswith('<![endif]'):
        if string.startswith('[if gte mso 9]>') \
        or string.startswith('[if !mso]>'):
            return True
    return False

def check_input_file_path(input_file_path):
    '''
    Check if the input file is ok,
    else print what’s wrong and exit with an error code.
    '''
    if os.path.samefile(input_file_path, __file__):
        print('It’s nonsense for this script to clean up itself.')
        return False
    if input_file_path.endswith('.py'):
        print('It’s nonsense for this script to clean up a Python script.')
        return False
    if input_file_path.endswith('.clean.html')\
    or input_file_path.endswith('.final.html'):
        print(
            'It’s nonsense for this script to clean up a file '
            'that has probably been cleaned up already. '
            'Its name ends with “.clean.html” or “.final.html”.'
        )
        return False
    if not os.path.exists(input_file_path):
        print('The file on the path given does not exist.')
        return False
    if not os.path.isfile(os.path.realpath(input_file_path)):
        print('The path specified does not represent a regular file.')
        if os.path.isdir(os.path.realpath(input_file_path)):
            print('Actually, it points to a directory.')
        return False
    return True


def entitatify(string):
    '''
    Entitatification:
    Replacement of the characters given by xml entities
    '''
    for c, ref in OrderedDict([
        ['&',        '&amp;'    ],
        ['<',        '&lt;'     ],
        ['>',        '&gt;'     ],
        ['\u00a0',   '&nbsp;'   ],
        ['\u2013',   '&ndash;'  ],
        ['\u2014',   '&mdash;'  ]
    ]).items():
        string = string.replace(c, ref)
    for c in constants.HIGH_CHARACTERS:
        string = string.replace(c, f'&#x{ord(c):x};')
    return string

try:
    ENTITATIFIER = bs4.formatter.HTMLFormatter(entitatify, indent=2)
except TypeError:
    ENTITATIFIER = bs4.formatter.HTMLFormatter(entitatify)
