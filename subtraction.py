class SubtractibleString(str):
    """
    A string that other string-like objects can be subtracted from
    using the standard “-” (“minus”) operator (from the right)
    or using the other methods defined here
    """
    def __sub__(self, other):
        return self.rstringstrip(other)

    def starts_or_ends_with_at_least_one_string(self, strings=None):
        """
        Tests if the SubtractibleString starts or ends 
        with at least one of the strings given.
        :strings: a list of strings to test
        """
        if strings is None:
            strings = []
        for s in strings:
            if self.endswith(s) \
            or self.startswith(s):
                return True
        return False

    def lstringstrip(self, string):
        """
        Strips a string given from the left (from the start)
        if this SubtractibleString starts with it.
        :string: a string to remove
        """
        result = self
        if result.startswith(string):
            result = SubtractibleString(self[len(string):])
        return result

    def rstringstrip(self, string):
        """
        Strips a string given from the right (from the end)
        if this SubtractibleString ends with it.
        :string: a string to remove
        """
        result = self
        if result.endswith(string):
            result = SubtractibleString(result[:-len(string)])
        return result

    def stringstrip(self, string):
        """
        Strips a string given from the left (from the start)
        or from the right (from the end) 
        if this SubtractibleString starts or ends with it.
        Both sides apply.
        :string: a string to remove
        """
        result = self.lstringstrip(string).rstringstrip(string)
        return result

    def stripall(self, strings=None):
        """
        Strips all of the string given from the left (from the start)
        or from the right (from the end) 
        if this SubtractibleString starts or ends with them.
        Both sides apply.
        Repeats until this SubtractibleString does neither start nor end
        with any of the strings given.
        :strings: a list of strings to remove
        """
        if strings is None:
            strings = []
        result = self
        go = result.starts_or_ends_with_at_least_one_string(strings)
        while go:
            for s in strings:
                result = result.stringstrip(s)
            go = result.starts_or_ends_with_at_least_one_string(strings)
        return result


class SubtractibleList(list):
    """
    A list other lists or other objects can be subtracted from.
    Lists get removed item by item,
    other types get removed as a single item.
    """
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def __sub__(self, other):
        if isinstance(other, list):
            return self.__class__(*[item for item in self if item not in other])
        else:
            return self.__class__(*[item for item in self if item != other])
