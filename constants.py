#!/usr/bin/env python

"""The HTML Cleaner common constants"""

CLASSES_TO_REMOVE = [
    "",
    "MsoNormal",
    "western",
    "WordSection1",
    "MsoListParagraph",
    "Standard",
    "normaltextrun",
    "scxw224061772",
    "Internet_20_link",
    "Tabulka1_C1",
    "Tabulka1_C2",
    "fr3",
    "moz-txt-link-abbreviated",
    "moz-txt-link-freetext",
    "moz-header-part1 moz-main-header",
    "moz-header-part2 moz-main-header",
    "moz-header-display-name",
    "moz-mime-attachment-header",
    "moz-text-plain",
    "moz-quote-pre",
    "moz-mime-attachment-header moz-print-only",
    "moz-mime-attachment-headerName moz-print-only",
    "moz-mime-attachment-header-name",
    "moz-mime-attachment-wrap moz-print-only",
    "moz-mime-attachment-table",
    "moz-mime-attachment-file",
    "moz-mime-attachment-size",
    "moz-attached-image",
    "moz-attached-image-container",
    "moz-text-flowed",
    "moz-signature",
    "-wm-MsoNormal",
    "-wm-MsoListParagraphCxSpFirst",
    "-wm-MsoListParagraphCxSpMiddle",
    "-wm-MsoListParagraphCxSpLast",
    "-wm-tw-data-text",
    "-wm-tw-text-large",
    "-wm-tw-ta",
    "-wm-western",
    "-wm-Y2IQFc",
    "-wm-moz-txt-link-freetext",
    "-wm-moz-signature",
    "Tabulka1",
    "Tabulka11",
    "Tabulka1_A1",
    "Subtitle",
    "ListLabel_20_1",
    "ListLabel_20_11",
    "odfLiEnd",
    "Body_20_Text_20_Indent",
    "gmail_signature",
    "gmail_quote gmail_quote_container",
    "gmail_attr",
    "gmail_sendername",
]
for CLASS_ALPHA_PART in ('P', 'T', 'fr', 'Tabulka'):
    CLASSES_TO_REMOVE += [
        f'{CLASS_ALPHA_PART}{ELNUM}' 
        for ELNUM in range (1, 21)
    ]

STYLES_TO_REMOVE = [
    # Align & size
    'text-align:left;width:7.502cm;',
    'text-align:left;width:1cm;',
    'text-align:left;width:7.5cm;',
    'vertical-align:baseline',
    # Margins
    'margin:0cm',
    'margin-top:0cm',
    'margin-top:0.42cm',
    'margin-top:0.11cm',
    'margin-top:0.21cm; margin-bottom:0cm',
    'margin-top:6.0pt',
    'margin-top:0.42cm; margin-bottom:0.21cm',
    'margin-top:0.11cm; margin-bottom:0.41cm',
    'margin-bottom:0.28cm',
    'margin-bottom:0.21cm',
    'margin-bottom:0cm',
    'margin-bottom:0.35cm',
    'margin-bottom:0.75cm',
    'margin-left:0cm',
    'margin-left:14.2pt',
    'margin-left:283.2pt',
    'margin-left:318.65pt',
    'margin-left:0.38cm; margin-bottom:0.28cm',
    'margin-left:0.38cm; margin-bottom:0cm',
    'margin-left:7.49cm; margin-bottom:0cm',
    'margin-left:1.12cm; margin-bottom:0.28cm',
    'margin-right:-7.1pt',
    'margin-right:0cm',
    # Padding
    'padding:0cm 0.19cm',
    'padding:0.1cm',
    'padding-top:0cm',
    'padding-top:0.1cm',
    'padding-bottom:0.1cm',
    'padding-left:0.1cm',
    'padding-right:0cm',
    'padding-right:0.1cm',
    'padding:0cm 0.19cm',
    # Display
    'display:block;float:left;min-width:0cm;',
    'display:inline',
    # Color
    'color:black',
    # Background
    'background:#E3E3E3',
    'background:white',
    'background-color:transparent',
    # Font
    'font-style:normal',
    'font-weight:normal',
    'font-variant:normal',
    'font-family: -moz-fixed; font-size: 12px',
    'font-variant-numeric:normal',
    'font-variant-east-asian:normal',
    'font-variant-alternates:normal',
    'font-size-adjust:none',
    'font-kerning:auto',
    'font-optical-sizing:auto',
    'font-feature-settings:normal',
    'font-variation-settings:normal',
    'font-variant-position:normal',
    'font-variant-emoji:normal',
    'font-stretch:normal',
    # Text style
    'text-decoration: none;;',
    'text-indent:-14.2pt',
    # Punctuation
    'punctuation-wrap:simple',
    # Spacing
    'letter-spacing:-0.5pt',
    'letter-spacing:normal',
    # Tabs
    'tab-stops:list 7.1pt',
    # Line height
    'line-height:normal',
    # Tables
    'border:none',
    'border:1px solid #000000',
    'border-top:1px solid #000000',
    'border-top:none',
    'border-bottom:1px solid #000000',
    'border-left:1px solid #000000',
    'border-left:solid #CCCCCC 1.0pt',
    'border-right:none',
    'border-right:1px solid #000000',
    # “Poor people”
    'orphans: 0',
    'widows: 0',
    # M$
    'mso-ligatures:none;mso-fareast-language:CS',
    'mso-fareast-language:CS',
    'mso-margin-top-alt:auto',
    'mso-margin-bottom-alt:auto',
    'mso-line-break-override:restrictions',
    'mso-add-space:auto',
    'mso-list:l1 level1 lfo1',
    'mso-list:l0 level1 lfo2'
]


FONT_SIZES_TO_REMOVE = {
    'pt':
        [
            7, 8, 9,
            10, 10.0, 11, 12, 12.0, 13, 14, 14.0, 15, 16, 18,
            20, 21, 22, 26, 28, 32
        ],
    'px':
        [
            12, 12.8, 16, 20, 24
        ]
}
def make_font_size_style_list(size_dict):
    """
    :sizes: a dictionary in the form {unit: size list, …}
    """
    result = []
    for unit, sizes in size_dict.items():
        for size in sizes:
            result.append(f'font-size: {size}{unit}')
    return result
FONT_SIZE_STYLES_TO_REMOVE = make_font_size_style_list(FONT_SIZES_TO_REMOVE)
STYLES_TO_REMOVE += FONT_SIZE_STYLES_TO_REMOVE

FONT_FACES_TO_REMOVE = [
    'Times New Roman, serif',
    'Times New Roman',
    'Liberation Serif, serif',
    'Calibri, serif',
    'inherit, serif',
    'AlgerianDOT',
    'AlgerianBasDOT',
    'Segoe UI',
]

FONT_FAMILY_STYLES_TO_REMOVE = [
    f'font-family: {F}' for F in FONT_FACES_TO_REMOVE + 
    [
        '"Verdana",sans-serif',
        'Verdana,sans-serif',
        '"Times New Roman",serif',
        '"Arial",sans-serif',
        '"Segoe UI Emoji",sans-serif',
        '"Aptos",sans-serif',
        'Calibri,sans-serif',
        "-moz-fixed",
        "Verdana,Geneva,sans-serif"
    ]
]
STYLES_TO_REMOVE += FONT_FAMILY_STYLES_TO_REMOVE

LINE_HEIGHTS_TO_REMOVE = [
    f'line-height:{height_to_remove}'
    for height_to_remove in ['100%', '108%', '120%']
]
STYLES_TO_REMOVE += LINE_HEIGHTS_TO_REMOVE

PAGEBREAK_STYLES = [
    f'page-break-{pos_kind}'
    for pos_kind in
    [
        'before:auto',
        'after:avoid'
    ]
]
STYLES_TO_REMOVE += PAGEBREAK_STYLES

HEADING_LEVELS = range(1, 7)

ATTRIBUTES_TO_REMOVE = {
    'border':       '0',
    'LANG':         None,
    'lang':         None,
    'bgcolor':      None,
    'data-email':   None,
    'dir':          None,
    'id':           '-wm-tw-target-text',
    'cols':         None,
}

SELECTORS_TO_UNWRAP = [
    'html',
    'head',
    'body',
    'fieldset',
    'legend',
    'h3 em',
    'em em',
    'font[color="#000000"]',
    'a[name]',
    r'o\:p',
    'link[rel="important stylesheet"]'
        '[href="chrome://messagebody/skin/messageBody.css"]',
    'link[href^="http://purl.org/dc/"]'
        '[hreflang="en"]'
        '[rel^="schema."]',
    'div[graphical-quote="true"]'
       '[wrap="true"]',
    'div[class="moz-text-html"]'
]

SELECTORS_TO_EXTRACT = [
    'meta',
    'link',
    'style',
    'script',
    'img[src^="imap-message://"]',
    'a[href="mailto:ZSsbory@evangnet.net"]',
    'a[href="https://konference.evangnet.cz/cgi-bin/mailman/listinfo/zssbory"]',
    'colgroup',
    'pre:-soup-contains("ZSsbory mailing list")',
]

WHITESPACE = [
    ' ', '\t', '\r', '\n', '\r\n', 
    '&nbsp;', '\u00a0', '\xa0'
]


HIGH_CHARACTERS = '😊🙂❤️'
# This takes a long time
# HIGH_CHARACTERS = ''.join((chr(C) for C in range(0x10000, 0x110000)))
