# HTML Cleaner

This is a custom HTML cleanup utility written in Python 3. 

- Used to remove certain CSS styles, classes, 
attributes, or tags from an HTML file on your filesystem.

- Saves the output as another HTML file with the `.clean.html` extension.

- Used primarily to remove nasty formatting from the source in order to create code that could be usable in a content management system (Drupal, Wordpress, ND:CMS…) or on another kind of website to keep a formatting graphically consistent with the rest of the site.

- Tailored strongly to the author’s needs.

- The approach is merely casuistical.

## Usage

```bash
python3 htmlcleaner.py </inupt/file/path.html>
```

This is basically what you get by typing

```bash
python3 htmlcleaner.py -h
```

or

```bash
python3 htmlcleaner.py --help
```


## Licence

This code is distributed under the terms of the **GNU General Public Licence, version 3.** A copy is included.

## Author

&#x1F12F; Pavel Řezníček 2023 (pavel.reznicek@evangnet.cz)