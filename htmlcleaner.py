#!/usr/bin/env python3

'''
HTML Cleaner
============

An utility for HTML cleanup from various kinds of untidy mess.
Mainly intended for the content management systems (CMS) 
that already have their own styling.
Any extra formatting in the code inserted into them
would break their visual style defined in their template being used
and induce awful inconsistencies.

Therefore we need this utility: 
To clean up the code to the lowest possible level
by removing any superfluous formatting.

The most common use is to remove Microsoft Office and LibreOffice-specific stuff
and also some elements of the e-mails exported from the Thunderbird client.

The approach is merely casuistic and author-centric. 😉
But the author hopes that it could also be useful for other people,
mutatis mutandis.
'''

import os
import sys
import argparse
import bs4

import utils
import subtraction

from constants import ATTRIBUTES_TO_REMOVE, CLASSES_TO_REMOVE, HEADING_LEVELS, \
    FONT_FACES_TO_REMOVE, STYLES_TO_REMOVE, \
    SELECTORS_TO_EXTRACT, SELECTORS_TO_UNWRAP, \
    WHITESPACE
from css import tag_has_style, filter_tag_style

class DeliciousSoup(bs4.BeautifulSoup): # pylint: disable=too-many-public-methods, abstract-method
    '''
    The class used for HTML cleanup
    '''
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        # Clean the soup up
        # This could be deferred to a separate call to the cleanup() method
        # but for now, we make things easy
        self.cleanup()

    def prettify(self, encoding=None, formatter=utils.ENTITATIFIER):
        '''
        Prettifies the DeliciousSoup and returns the pretty soup in a string.
        '''
        result = super().prettify(encoding, formatter)
        return result

    def extract_selector(self, selector):
        '''
        Extract tags by the selector given
        '''
        elements = self.select(selector)
        for element in elements:
            element.extract()

    def extract_selectors(self, selectors):
        '''
        Extract tags by all of the selectors given
        '''
        for selector in selectors:
            self.extract_selector(selector)

    def unwrap_selector(self, selector):
        '''
        Unwrap tags by the selector given
        '''
        elements = self.select(selector)
        for element in elements:
            element.unwrap()

    def unwrap_selectors(self, selectors):
        '''
        Unwrap tags by all of the selectors given
        '''
        for selector in selectors:
            self.unwrap_selector(selector)

    def remove_classes(self, classes):
        '''
        Remove 'class="… x …"'
        '''
        for cssclass in classes:
            c2r_selector = f'[class="{cssclass}"]'
            c2r_elements = self.select(c2r_selector)
            c2r_list = cssclass.split(' ')
            for element in c2r_elements:
                classes = element['class']
                for c2r_list_item in c2r_list:
                    if c2r_list_item in classes:
                        classes.remove(c2r_list_item)
                if len(classes) == 0:
                    del element['class']

    def select_elements_with_style(self, css):
        '''
        Select all elements in a BeautifulSoup with the style given as string: 
        A shorthand.
        The css can be cluttered a bit (comments, whitespace, semicolons).
        Returns elements with all the properties defined in the css argument.
        '''
        result = [
            el for el in self.find_all() if tag_has_style(el, css)
        ]
        return result

    def remove_style(self, style):
        '''
        Remove 'style="* x *"'
        '''
        s2r_elements = self.select_elements_with_style(style)
        for element in s2r_elements:
            # Remove the style fragment/css property
            # from the style attribute
            filter_tag_style(element, style)

    def remove_styles(self, styles):
        '''
        Remove 'style="* x *"'
        '''
        for style in styles:
            self.remove_style(style)

    def unformat_links(self):
        '''
        Remove coloring and underlinig from links
        '''
        font_u_links = self.select('font[color="#0563c1"] u a[href]')
        for link in font_u_links:
            font = link.parent.parent
            underline = link.parent
            underline.unwrap()
            font.unwrap()

    def emphasize(self):
        '''
        Replace raw yelling formats (<u>, <b>, and <i>) with emphasis
        '''
        elements = self.select('u,b,i')
        for element in elements:
            element.name = 'em'

    def remove_all_styles_from_headings(self):
        '''
        Remove all styles from headings
        '''
        heading_selector_list = [f'h{h}[style]' for h in HEADING_LEVELS]
        heading_selectors = ', '.join(heading_selector_list) # 'h1[style],...'
        heading_elements = self.select(heading_selectors)
        for element in heading_elements:
            del element['style']

    def remove_spacing_spans_from_headings(self):
        '''
        Remove spacing spans from headings
        '''
        heading_spacing_selector_list = [
            f'h{h} span[style="letter-spacing:2.6pt"]' for h in HEADING_LEVELS
        ]
        heading_spacing_selectors = ', '.join(heading_spacing_selector_list)
        heading_spacing_elements = self.select(heading_spacing_selectors)
        for elem in heading_spacing_elements:
            elem.unwrap()

    def remove_font_size_attributes(self):
        '''
        Remove <font size="*"></font> elements
        '''
        font_size_elements = self.select('font[size]')
        for elem in font_size_elements:
            del elem['size']

    def remove_font_face_attributes(self, faces):
        '''
        Remove <font face="*"></font> attributes
        '''
        font_face_selector = ','.join(
            [
                f'font[face="{face}"]'
                for face in faces
            ]
        )
        font_face_elements = self.select(font_face_selector)
        for elem in font_face_elements:
            del elem['face']

    def stylify_font_colors(self):
        """
        Replace <font color="*"></font> elements
        with <span style="color: *"></span>
        """
        font_color_selector = 'font[color]'
        font_color_elements = self.select(font_color_selector)
        for element in font_color_elements:
            color = element.color
            element.name = 'span'
            element.style = f'color: {color}'

    def enrich_poor_people(self):
        '''
        Remove widows/orphans styles
        '''
        poor_people = ('widows', 'orphans')
        for social_status in poor_people:
            poor_people_selector = f'span[style*="{social_status}"]'
            poor_people_elements = self.select(poor_people_selector)
            for element in poor_people_elements:
                filter_tag_style(element, social_status)

    def aligns_to_styles(self):
        '''
        Replace all the 'align="xxx"' attributes by corresponding styles
        '''
        for align in ('left', 'right', 'center', 'justify'):
            self.align_to_style(align)

    def stylify_strikes(self):
        '''
        Replace the <strike></strike> elements
        with <span style="text-decoration: line-through"></span>
        '''
        strike_elements = self.find_all('strike')
        for element in strike_elements:
            element.name = 'span'
            element['style'] = 'text-decoration: line-through'

    def extract_avg_tables(self):
        '''
        Remove the AVG ad tables
        (one ore more if present)
        '''
        # Find the characteristical links
        selector = \
            'div table tbody td p a[href^="http://www.avg.com/email-signature"]'
        avg_links = self.select(selector)
        # Go trough the links
        for avg_link in avg_links: # just in case we have more
            if avg_link:
                # Find the enclosing div tag
                root_div = avg_link.find_parent('div')
                # Extract it
                if root_div:
                    root_div.extract()

    def unwrap_attributeless_elements(self, selector):
        '''
        Unwrap elements without attributes
        Typically used for span or font tags 
        that don’t carry any more information
        (the attributes got cleared previously)
        '''
        elements = self.select(selector)
        for elem in elements:
            attributes = elem.attrs
            if len(attributes) == 0:
                elem.unwrap()

    def align_to_style(self, align):
        '''Replace the 'align="xxx"' attribute by a style'''
        align_elements = self.select(f'[align="{align}"]')
        for elem in align_elements:
            del elem['align']
            if elem.has_attr("style"):
                elem["style"] += '; '
            else:
                elem["style"] = ''
            elem["style"] += 'text-align: ' + align

    def extract_empty_comments(self):
        '''
        Remove empty comments
        '''
        comments = self.find_all(
            string=lambda text: isinstance(text, bs4.Comment)
        )
        for comment in comments:
            subtractible_comment = subtraction.SubtractibleString(comment)
            if subtractible_comment.stripall(WHITESPACE) == '':
                comment.extract()

    def extract_mso_conditional_code(self):
        '''
        Remove MSO conditional code
        '''
        comments = self.find_all(
            string=lambda text: isinstance(text, bs4.Comment)
        )
        for comment in comments:
            if utils.comment_is_mso_conditional_code(comment):
                comment.extract()

    def remove_attribute(self, name, value=None):
        '''
        Remove the attribute given from all elements.
        An attribute value can be specified; 
        if None or empty it matches any value.
        '''
        value_part = f'="{value}"' if value else ''
        elements = self.select(f'[{name}{value_part}]')
        for element in elements:
            del element[name]

    def remove_attributes(self, attributes):
        '''
        Remove the attributes given from all tags
        :attributes: A dictionary of attribute name: value pairs.
            If a value is None or empty it matches any value.
        '''
        for name, value in attributes.items():
            self.remove_attribute(name, value=value)

    def remove_empty_elements(self, selector):
        '''
        Remove empty elements — typically used for divs and paragraphs
        '''
        elements = self.select(selector)
        for elem in elements:
            # Consider tags without attributes only
            hasattrs = utils.tag_has_attributes(elem)
            if not hasattrs:
                contains_nonbreaks = utils.tag_contains_nonbreaks(elem)
                # Allow non-blank text
                # (blank text consists of whitespace and non-breaking spaces
                if contains_nonbreaks:
                    extract = utils.tag_text_contains_nonwhitespace(elem)
                else:
                    extract = contains_nonbreaks
                # If the tag should be removed then remove it
                if extract:
                    elem.extract()

    def title_to_comment(self):
        '''
        Convert the title to a comment
        '''
        title = self.title
        if title:
            titlestring = title.string
            if titlestring:
                comment = bs4.Comment(title.string)
                title.replace_with(comment)
            else:
                title.extract()

    def cleanup(self):
        '''
        The main part of the HTML Cleaner project.
        Cleans up the markup contained in this DeliciousSoup.
        '''
        print("Starting the cleanup core…")
        # Remove listed elements (generally)
        print("Removing listed elements (generally)…")
        self.extract_selectors(SELECTORS_TO_EXTRACT)
        # Unwrap listed elements (generally)
        print("Unwrapping listed elements (generally)…")
        self.unwrap_selectors(SELECTORS_TO_UNWRAP)
        # Remove 'class="* x *"'
        print("Removing listed CSS classes…")
        self.remove_classes(CLASSES_TO_REMOVE)
        # Remove 'style="* x *"'
        print("Removing listed inline CSS styles…")
        self.remove_styles(STYLES_TO_REMOVE)
        # Unformat links
        print("Unformatting links…")
        self.unformat_links()
        # # Replace <b>, <u>, and <i> with <em>
        # print("Replacing <b>, <u>, and <i> with <em>…")
        # self.emphasize()
        # Remove styles from headings
        print("Removing all styles from the headings…")
        self.remove_all_styles_from_headings()
        # Remove spacing spans from headings
        print("Removing line spacing spans from the headings…")
        self.remove_spacing_spans_from_headings()
        # Remove <font size="*"></font> attributes
        print('Removing <font size="*"></font> attributes')
        self.remove_font_size_attributes()
        # Remove <font face="*"></font> attributes
        print('Removing <font face="*"></font> attributes')
        self.remove_font_face_attributes(FONT_FACES_TO_REMOVE)
        # Replace <font color="*"></font> elements
        # with <span style="color: *"></span>
        print('Stylifying font colors…')
        self.stylify_font_colors()
        # Remove widows/orphans styles
        print("Enriching poor people…")
        self.enrich_poor_people()
        # Replace the 'align="xxx"' attributes by corresponding styles
        print("Stylifying text aligns…")
        self.aligns_to_styles()
        # Replace the <strike></strike> elements
        # with <span style="text-decoration: line-through"></span>
        print("Stylifying strikes…")
        self.stylify_strikes()
        # Remove the AVG ad table if present
        print("Removing the AVG ad table if present…")
        self.extract_avg_tables()
        # Remove empty comments
        print("Removing empty comments…")
        self.extract_empty_comments()
        # Remove MSO conditional code
        print("Removing the MSO conditional code…")
        self.extract_mso_conditional_code()
        # Remove listed attributes from all tags
        print("Removing listed attributes from all tags…")
        self.remove_attributes(ATTRIBUTES_TO_REMOVE)
        # Eventually remove spans and fonts without attributes
        print("Removing spans and fonts without attributes…")
        self.unwrap_attributeless_elements('span,font')
        # Remove empty elements without attributes
        print("Removing empty elements without attributes…")
        self.remove_empty_elements('p,tr,div')
        # Convert the title to a comment
        print("Converting the title to a comment…")
        self.title_to_comment()
        print("The cleanup core done.")


# The core
def cleanup(input_file_path):
    """
    The reason for the existence of this module. 🙂
    The cleanup core.
    """
    # If the file doesn’t exist, no music.
    path_is_ok = utils.check_input_file_path(input_file_path)

    if not path_is_ok:
        return False

    #########
    # Input #
    #########

    # Read and parse the input file
    print("Reading and decoding the input file…")
    with open(input_file_path, 'rb') as infile:
        data = infile.read()
    markup = bs4.UnicodeDammit.detwingle(data)

    ###################
    # Transformations #
    ###################

    dsoup = DeliciousSoup(
        markup,
        #features='html5lib',
        features='lxml',
        from_encoding='utf-8'
    )

    ##########
    # Output #
    ##########

    # Save the result
    print("Saving the result…")
    output_file_path = '.clean'.join(os.path.splitext(input_file_path))
    output_string = dsoup.prettify()
    with open(output_file_path, 'w', encoding='utf-8') as outfile:
        outfile.write(output_string)
    print(f"Output written to file “{output_file_path}”.")

    # Success
    return True

# On module run do this:
if __name__ == '__main__':
    PARSER = argparse.ArgumentParser()
    PARSER.add_argument('input_file_path')
    ARGS = PARSER.parse_args()

    INPUT_FILE_PATH = ARGS.input_file_path

    SUCCESS = cleanup(INPUT_FILE_PATH)

    if not SUCCESS:
        sys.exit(1)
