"""
A module for CSS comparison, parsing, and declaration removal.
"""

import tinycss2

def are_declaration_values_equal(val1, val2):
    '''CSS declaration values comparison'''
    # Filter out the garbare (comments and whitespace)
    # in order to compare the two CSS property values concisely
    token_types_to_ignore = (tinycss2.ast.Comment, tinycss2.ast.WhitespaceToken)
    clean_val1 = [t for t in val1 if type(t) not in token_types_to_ignore]
    clean_val2 = [t for t in val2 if type(t) not in token_types_to_ignore]
    # Compare token after token
    for t, token1 in enumerate(clean_val1):
        # A magical method to get the second token or None if not present
        token2 = dict(enumerate(clean_val2)).get(t)
        # If the two tokens are both Nodes and they differ
        # then the whole values differ → return False
        if token1.__class__ == token2.__class__ and \
        isinstance(token1, tinycss2.ast.Node):
            # We need to serialize the tokens (make them strings)
            # beacause they are mutable objects and so can differ
            # even if they contain the same text
            if token1.serialize() != token2.serialize():
                return False
        # And if the token types differ
        # then the whole values also differ → return False
        else:
            return False
    # If we got here without any difference
    # then the values are equal
    return True

def do_declaration_values_match(val1, val2):
    '''
    CSS declaration values matching
    If decl2.value is None it matches everything
    '''
    equal = are_declaration_values_equal(val1, val2)
    result = equal or (val2 is None)
    return result

def are_declarations_equal(decl1, decl2, match_none_val = False):
    '''CSS declarations comparison or matching'''
    if match_none_val:
        match = do_declaration_values_match(decl1.value, decl2.value)
    else:
        match = are_declaration_values_equal(decl1.value, decl2.value)
    result = (
        isinstance(decl1, tinycss2.ast.Declaration) and
        isinstance(decl2, tinycss2.ast.Declaration) and
        decl1.name == decl2.name and
        match
    )
    return result

def do_declarations_match(decl1, decl2):
    '''CSS declarations matching'''
    result = are_declarations_equal(decl1, decl2)
    return result


class CSSDeclarationList(list):
    '''A more intelligent list to manage CSS style declarations'''
    def __init__(self, declaration_list):
        super().__init__()
        # If we were given a string/bytes object let’s parse it
        if isinstance(declaration_list, str) \
        or isinstance(declaration_list, bytes):
            real_declaration_list = \
                tinycss2.parse_declaration_list(declaration_list)
        # If we were given a single declaration object
        elif isinstance(declaration_list, tinycss2.ast.Declaration):
            real_declaration_list = \
                [declaration_list]
        # If we got a None
        elif declaration_list is None:
            real_declaration_list = []
        # Else take it as is
        else:
            real_declaration_list = declaration_list
        # Load all list items = make ourselves a copy
        for d in real_declaration_list:
            self.append(d)

    def __sub__(self, other):
        '''Subtraction'''
        # Make a copy
        result = CSSDeclarationList(self.copy())
        # If we are going to subtract a CSS declaration list
        if isinstance(other, list):
            # Subtract each declaration in the other list
            for d in other:
                result = result.__sub__(d)
        # If we are going to subtract something else
        # (typically a tinycss2.ast.Declaration)
        elif isinstance(other, tinycss2.ast.Declaration):
            # If a declaration is equal to the other
            # then remove it
            for d in self:
                if do_declarations_match(d, other):
                    result.remove(d)
                    # If a whitespace token follows then remove it
                    if len(result) > 0:
                        if isinstance(result[0], tinycss2.ast.WhitespaceToken):
                            result.pop(0)
        else:
            pass # Keep the result intact (don’t subtract anything)
            #raise ValueError(
            #    f"The '{other.__class__.__name__}' "
            #    f"object cannot get subtracted from a CSSDeclarationlist"
            #)
        return result

    def contains_declaration(self, declaration_sought):
        '''Check if the list contains an exact declaration given'''
        for d in self:
            if isinstance(d, tinycss2.ast.Declaration):
                if are_declarations_equal(d, declaration_sought):
                    return True
        return False

    def contains_all_declarations(self, declarations_sought):
        '''
        Check if the list contains all exact declarations in the list given
        '''
        for d in declarations_sought:
            if not self.contains_declaration(d):
                return False
        return True


def remove_css_declarations(source, css_to_remove):
    '''
    Remove the CSS properties given from an inline style string
    (style="xxx; yyy; zzz": remove yyy)
        :source:
            - a "style" attribute of an HTML element
        :css_to_remove: 
            - several properties that should be removed
            - a CSS expression that gets parsed
    '''

    # Parse the property declarations in the source
    decls = CSSDeclarationList(source)

    # Parse the property declarations to remove
    decls2r = CSSDeclarationList(css_to_remove)

    # Use our magical subtraction operator ;-)
    new_decls = decls - decls2r

    # Return the filtered source as a string
    result = tinycss2.serialize(new_decls)
    return result

def tag_has_style(tag, css):
    '''Check if the bs4 tag given contains the css declarations given'''
    tag_css = tag.get("style")
    tag_dlist = CSSDeclarationList(tag_css)
    target_dlist = CSSDeclarationList(css)
    result = tag_dlist.contains_all_declarations(target_dlist)
    return result

def filter_tag_style(tag, css_to_remove):
    '''
    Remove the CSS properties given from an inline style attibute
    of the tag given
    (style="xxx; yyy; zzz": remove yyy)
    - a shorthand featuring the remove_css_declarations() function
    :tag:
        - a bs4 tag element the style attribute of which
            should be cleaned up
    :css_to_remove:
        - several properties that should be removed
        - a CSS expression that gets parsed
    :return: None, the tag style gets modified in-place
    '''
    old_style = tag['style']
    if old_style:
        new_style = remove_css_declarations(old_style, css_to_remove)
        if new_style > '':
            tag['style'] = new_style
        else:
            del tag['style']
